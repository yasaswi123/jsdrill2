//Implement a loop to access and log the city and country of each individual in the dataset.

function problem8(arrayOfObjects){
    let details_array=[];
    for(let index = 0; index < arrayOfObjects.length ; index++){
        if(arrayOfObjects[index].city != undefined && arrayOfObjects[index].country != undefined){
            details_array.push(`city is ${arrayOfObjects[index].city} and country is ${arrayOfObjects[index].country} `);
        }
        else{
            return "Values are undefined";
        }
    }
    return details_array;
}

module.exports = problem8;