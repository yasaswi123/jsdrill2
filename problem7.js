//  Write a function that accesses and prints the names and email addresses of individuals aged 25.

function problem7(arrayOfObjects){
    let array_age=[];
    for(let index = 0 ; index < arrayOfObjects.length ; index++){
        if(arrayOfObjects[index].age == 25){
            array_age.push(`Name is ${arrayOfObjects[index].name} and Email is ${arrayOfObjects[index].email}`);
        }
    }
    if(array_age.length > 0){
        return array_age;
    }
    else{
        return "No person with age 25";
    }
}

module.exports = problem7;