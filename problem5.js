// Implement a loop to access and print the ages of all individuals in the dataset.

function problem5(arrayOfObjects){
    let age_array = [];
    if(arrayOfObjects.length > 0){
        for(let index=0 ; index < arrayOfObjects.length ; index ++){
            age_array.push(arrayOfObjects[index].age);
        }
        return age_array;
    }
    else{
        return "Array is empty";
    }
}

module.exports = problem5;
  