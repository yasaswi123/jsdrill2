//    Create a function that extracts and displays the names of individuals who are students (`isStudent: true`) and live in Australia.

function problem3(arrayOfObjects){
    for(let index = 0 ; index < arrayOfObjects.length ; index++){
        if(arrayOfObjects[index].isStudent && arrayOfObjects[index].country === 'Australia'){
            console.log(arrayOfObjects[index].name);
        }
    }
}

module.exports = problem3;
