// Write a function that accesses and logs the name and city of the individual at the index position 3 in the dataset.

function problem4(arrayOfObjects){
    if(arrayOfObjects.length > 0){
        console.log(` Name is ${arrayOfObjects[2].name} and City is ${arrayOfObjects[2].city}`);
    }
    else{
        console.log("Array is empty");
    }
}

module.exports = problem4;